import logging

from bs4 import BeautifulSoup

from spiderino import Spider, Item, URL
from spiderino.fields import IntegerField, StringField


class NewsItem(Item):
    title = StringField()
    url = StringField()
    points = IntegerField()


class HackerNewsSpider(Spider):
    name = "HackerNews"
    start_urls = [URL("https://news.ycombinator.com/news")]

    async def process(self, response):
        html = await response.text()
        soup = BeautifulSoup(html, "html.parser")

        # We follow the next url as soon as possible to keep the crawler busy.

        # Beautiful Soup will give us a relative url. Before following it we
        # need to join with the current url from the response.
        try:
            _next = soup.select(".morelink")[0].get("href")
            _next = response.url.join(URL(_next))
            self.follow(_next)
        except IndexError:
            logging.info("Cannot find next url to crawl.")

        # HackerNews submissions are inside one table. But each one is occupy
        # 3 rows.
        rows = soup.select(".itemlist")[0].findAll("tr")

        # We create a item object for each submission and yield it back to
        # the crawler.
        for i in range(0, len(rows) - 3, 3):
            item = NewsItem()
            item.title = rows[i].findAll("td")[2].find("a").string
            item.url = rows[i].findAll("td")[2].find("a")["href"]
            item.points = int(
                rows[i + 1].findAll("td")[1].find("span").string.split(" ")[0]
            )

            yield item


# We don't have a configuration schema or a defaults defined yet but this
# one show be ok for a long time.
config = {
    "DOWNLOADER": "spiderino.downloader.Downloader",
    "MIDDLEWARE": [
        "spiderino.middleware.download.DuplicatedUrlMiddleware",
        "spiderino.middleware.download.RobotsTxtMiddleware",
    ],
    "PIPELINE": [
        # Pipelines are useful to export items. The one bellow export all
        # items to one .csv file.
        "spiderino.pipelines.CsvExporter"
    ],
    "THROTTLE_ENABLED": True,
    "THROTTLE_MAX_CONNECTIONS": 1,
    "THROTTLE_DOWNLOAD_DELAY": 5,
}

if __name__ == "__main__":
    # Setup logging so we see things happening.
    logging.basicConfig(level=logging.INFO)

    # Import the crawler, set it up and start.
    from spiderino import Crawler

    c = Crawler(config=config)
    c.add_spider(HackerNewsSpider())
    c.run()
