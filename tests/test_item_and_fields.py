import pytest

from spiderino.item import Item
from spiderino.fields import BooleanField, FloatField, IntegerField, StringField


@pytest.fixture
def item():
    class TestItem(Item):
        boolfield = BooleanField()
        floatfield = FloatField()
        intfield = IntegerField()
        strfield = StringField()

    return TestItem()


def test_fields(item):
    item.floatfield = 1.0
    assert item.floatfield == 1.0
    assert isinstance(item.floatfield, float)

    item.boolfield = True
    assert item.boolfield is True
    assert isinstance(item.boolfield, int)

    item.intfield = 5
    assert item.intfield == 5
    assert isinstance(item.intfield, int)

    item.strfield = "name"
    assert item.strfield == "name"
    assert isinstance(item.strfield, str)


def test_item_dict(item):
    item.boolfield = False
    item.intfield = 20
    item.strfield = "string"

    assert item.__dict__ == {"boolfield": False, "intfield": 20, "strfield": "string"}
