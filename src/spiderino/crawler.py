import asyncio
import datetime
import logging
import sys
from asyncio import get_event_loop, Future
from typing import TYPE_CHECKING, Any, Dict, List, Optional, Union

from spiderino.exceptions import AbortRequest, DropItem
from spiderino.middleware import BaseMiddleware
from spiderino.pipelines import BasePipeline
from spiderino.utils import import_from_str

if TYPE_CHECKING:
    from asyncio import AbstractEventLoop
    from spiderino import Spider
    from spiderino.downloader import Downloader
    from yarl import URL


BASE_CONFIGURATION = {
    "DOWNLOADER": "spiderino.downloader.Downloader",
    "MIDDLEWARE": [
        "spiderino.middleware.download.DuplicatedUrlMiddleware",
        "spiderino.middleware.download.RobotsTxtMiddleware",
    ],
    "PIPELINE": ["spiderino.pipelines.CsvExporter"],
    "THROTTLE_ENABLED": True,
    "THROTTLE_MAX_CONNECTIONS": 2,
}


class Crawler:
    def __init__(self, config=None, loop: Optional["AbstractEventLoop"] = None) -> None:
        self.loop = loop or get_event_loop()
        self.config: Dict[str, Any] = self.prepare_config(config)

        self.spiders: Dict[str, Spider] = {}
        self.downloader: Downloader = None
        self.middlewares: List["BaseMiddleware"] = []
        self.pipelines: List["BasePipeline"] = []
        self.futures: List[Future] = []
        self.statistics: Dict[str, Any] = {}

        self.setup_downloader()
        self.setup_middleware()
        self.setup_pipeline()
        self.setup_statistics()

    def prepare_config(self, config):
        if config is None:
            logging.warning("Crawler did not receive configuration!")
            return BASE_CONFIGURATION
        else:
            return config

    def setup_downloader(self):
        try:
            cls = import_from_str(self.config["DOWNLOADER"])
        except ImportError:
            raise AttributeError
        self.downloader = cls(self.loop, self.config)

    def setup_middleware(self):
        for m in self.config["MIDDLEWARE"]:
            try:
                self.add_middleware(m)
            except ImportError:
                logging.warning(f"Failed to import {m}")

    def setup_pipeline(self):
        for p in self.config["PIPELINE"]:
            try:
                self.add_pipeline(p)
            except ImportError:
                logging.warning(f"Failed to import {p}")

    def setup_statistics(self):
        self.statistics: dict = {"urls": 0, "items": 0, "dropped": 0, "exported": 0}

    def add_middleware(self, middleware: Union[str, BaseMiddleware]):
        if isinstance(middleware, str):
            cls = import_from_str(middleware)
            middleware = cls(self.config, self.loop)

        if not isinstance(middleware, BaseMiddleware):
            raise ValueError

        self.middlewares.append(middleware)

    def add_pipeline(self, pipeline: Union[str, BasePipeline]):
        if isinstance(pipeline, str):
            cls = import_from_str(pipeline)
            pipeline = cls(self.config, self.loop)

        if not isinstance(pipeline, BasePipeline):
            raise ValueError

        self.pipelines.append(pipeline)

    def add_spider(self, spider: "Spider"):
        if spider.name not in self.spiders:
            self.spiders[spider.name] = spider
            spider.set_crawler(self)

    async def start(self, future: asyncio.Future) -> None:
        """Start the crawling process and act as a manager task."""
        self.statistics["start-time"] = datetime.datetime.now()

        for spider in self.spiders:
            for url in self.spiders[spider].start_urls:
                self.crawl(url, self.spiders[spider])

        while True:
            for f in self.futures:
                if f.done() or f.cancelled():
                    self.futures.remove(f)

            if len(self.futures) == 0:
                break

            await asyncio.sleep(3)

        self.close()
        future.set_result("Done crawling.")

    async def fetch(self, url: "URL", spider: "Spider") -> None:
        """Fetch pages and apply middlewares to the request."""
        try:
            for middleware in self.middlewares:
                await middleware.process_request(url, self.loop)

            response = await self.downloader.fetch(url)
            self.statistics["urls"] += 1

            for middleware in self.middlewares:
                await middleware.process_response(response, self.loop)
        except AbortRequest as e:
            logging.info(f"Request aborted: {e}")
        except Exception as e:
            # Probably not severe enough to crash but we still want to log
            logging.info(f"{e} | {url}")
        else:
            f = asyncio.ensure_future(self.process(response, spider), loop=self.loop)
            self.futures.append(f)

    async def process(self, response, spider: "Spider") -> None:
        """Extracts items to be exported and apply pipelines to them."""
        async for item in spider.process(response):
            try:
                for pipeline in self.pipelines:
                    await pipeline.process_item(item, spider)
            except DropItem:
                self.statistics["dropped"] += 1
            else:
                self.statistics["items"] += 1

    def crawl(self, url: "URL", spider: "Spider"):
        f = asyncio.ensure_future(self.fetch(url, spider), loop=self.loop)
        self.futures.append(f)

    def close(self):
        self.downloader.close()

        for pipeline in self.pipelines:
            pipeline.close()

    @property
    def elapsed_time(self):
        return datetime.datetime.now() - self.statistics["start-time"]

    def run(self) -> None:
        """Shortcut to run the crawler without much setup."""
        try:
            future = asyncio.Future()
            self.loop.run_until_complete(self.start(future))
        except KeyboardInterrupt:
            sys.stderr.flush()
            logging.info(f"Received keyboard interrupt.")
        except Exception as e:
            logging.info(e)
        finally:
            self.close()
            logging.info(
                f"Crawled {self.statistics['urls']} urls "
                f"and {self.statistics['items']} items. "
                f"Finished in {self.elapsed_time}."
            )
