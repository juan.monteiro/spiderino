class AbortRequest(Exception):
    """Abort a request before the downloader fetch it."""
    pass


class ConfigurationError(Exception):
    """Raised when there's a error in the configuration."""


class DropItem(Exception):
    """Drop the item for some reason."""
    pass
