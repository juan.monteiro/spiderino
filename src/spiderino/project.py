class Project:
    """A Project is composed of one or more Spiders and a configuration file.
    """

    def __init__(self):
        pass

    @staticmethod
    def load(path: str = None):
        """Load and return a project from a path.

        :param path: path of the project
        """
        pass

    def load_config(self):
        pass
