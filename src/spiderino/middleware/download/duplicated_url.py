from spiderino.exceptions import AbortRequest
from spiderino.middleware import BaseMiddleware


class DuplicatedUrlMiddleware(BaseMiddleware):
    """Avoid crawling the same page more than once."""
    seen_urls = []

    async def process_request(self, url, loop):
        if str(url) in self.seen_urls:
            raise AbortRequest(f"Blocked by DuplicatedUrl middleware. {url}")
        else:
            self.seen_urls.append(str(url))
