from spiderino.middleware.download.duplicated_url import DuplicatedUrlMiddleware
from spiderino.middleware.download.robots_txt import RobotsTxtMiddleware

__all__ = ['DuplicatedUrlMiddleware', 'RobotsTxtMiddleware']