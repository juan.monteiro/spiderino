from urllib.robotparser import RobotFileParser

from yarl import URL

from spiderino.base import BaseMiddleware
from spiderino.exceptions import AbortRequest


class RobotsTxtMiddleware(BaseMiddleware):
    """Verify if a url/request should be allowed by the 'robot.txt' rules."""
    robots = dict()

    async def process_request(self, url, loop):
        if url.host not in self.robots:
            self.parse_robots(URL.build(scheme=url.scheme, host=url.host, path='robots.txt'))

        if not self.robots[url.host].can_fetch("SPIDERINO/Crawler", str(url)):
            raise AbortRequest(f"Request blocked by robots.txt rules. {url}")

    def parse_robots(self, url):
        self.robots[url.host] = RobotFileParser(str(url))
        self.robots[url.host].read()
