from abc import ABCMeta


class BaseMiddleware(metaclass=ABCMeta):
    """Base Download Middleware

    Args:
        config (dict): the crawler configuration dict.
        loop: the current event loop.
        """

    def __init__(self, loop=None, config=None):
        pass

    async def process_request(self, url, loop):
        pass

    async def process_response(self, url, loop):
        pass


__all__ = ['BaseMiddleware']
