import logging
from asyncio import Semaphore, sleep
from datetime import datetime, timedelta
from typing import TYPE_CHECKING, Any, Dict

from aiohttp import ClientSession

if TYPE_CHECKING:
    from asyncio import AbstractEventLoop
    from aiohttp.web import Response
    from yarl import URL


class Downloader:
    """Aiohttp download handler.

    Args:
        loop: The current event loop for this app
        config: The current configuration
    """
    def __init__(self, loop: 'AbstractEventLoop', config: Dict[str, Any]) -> None:
        self.loop = loop
        self.session = ClientSession(loop=loop)
        self.config = config

        # throttle configuration
        self.semaphore: Dict[str, Semaphore] = dict()
        self.last_request: Dict[str, datetime] = dict()
        if 'THROTTLE_DOWNLOAD_DELAY' in self.config:
            self.delay = timedelta(seconds=int(self.config['THROTTLE_DOWNLOAD_DELAY']))
        else:
            self.delay = timedelta(seconds=5)

    async def fetch(self, url: 'URL'=None, retry: int=0) -> 'Response':
        """
        Args:
            url (URL): the url to be fetched.
            retry (int): number of times to retry if it fails.
        """
        if self.config['THROTTLE_ENABLED']:
            if url.host not in self.semaphore:
                self.semaphore[url.host] = Semaphore(int(self.config['THROTTLE_MAX_CONNECTIONS']), loop=self.loop)
                self.last_request[url.host] = datetime.utcnow() - timedelta(seconds=100)

            # Check if we can continue with this request or need to wait
            while (datetime.utcnow() - self.last_request[url.host]) < self.delay:
                await sleep(1)
            # Now that we can proceed we need to grab the lock
            await self.semaphore[url.host].acquire()

        logging.info(f'Fetching: {url}')
        response = await self.session.get(url)

        # now we should save the datetime that this request was completed and release the lock
        if self.config['THROTTLE_ENABLED']:
            self.last_request[url.host] = datetime.utcnow()
            self.semaphore[url.host].release()

        return response

    def close(self) -> None:
        self.session.close()


__all__ = ['Downloader']
