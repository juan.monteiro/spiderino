"""
    spiderino.base
    ~~~~~~~~~~~~~~

    This module contains is the foundation of this library and is where people
    looking to expand it should start.

    :copyright: (c) 2017 by Juan Carlo Domingues Monteiro.
    :license: MIT, see LICENSE for more details.
"""

from spiderino.middleware import BaseMiddleware
from spiderino.pipelines import BasePipeline

__all__ = ['BaseMiddleware', 'BasePipeline']
