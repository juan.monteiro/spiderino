import csv
from datetime import datetime
from typing import TYPE_CHECKING

from spiderino.pipelines import BasePipeline

if TYPE_CHECKING:
    from spiderino import Item, Spider


# TODO: define configuration structure
# TODO: load configuration
# TODO: handle file name
class CsvExporter(BasePipeline):
    """Export items to a csv file.

    Args:
        encoding (str): encoding that will be used, default to utf-8.
    """

    def __init__(self, loop, config, **kwargs) -> None:
        super().__init__(loop, config)
        self._set_config(config, None)
        self.headers = False
        self.writer = None

    def _set_config(self, config, spider):
        self.filename = f"result - {datetime.now()}.csv"
        self.file = open(self.filename, "w", newline="")
        pass

    async def process_item(self, item: "Item", spider: "Spider") -> None:
        if self.writer is None:
            self.writer = csv.DictWriter(self.file, fieldnames=item.__dict__.keys())

        if not self.headers:
            self.writer.writeheader()
            self.headers = True

        self.writer.writerow(item.__dict__)

    def close(self) -> None:
        self.file.close()
