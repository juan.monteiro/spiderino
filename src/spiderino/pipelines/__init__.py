from abc import ABCMeta, abstractmethod
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from spiderino.item import Item
    from spiderino.spider import Spider


class BasePipeline(metaclass=ABCMeta):
    """Base item pipeline."""

    def __init__(self, loop, config):
        self._loop = loop

    @abstractmethod
    async def process_item(self, item: 'Item', spider: 'Spider') -> None:
        """Process item from the spider.

        Args:
            item (Item): item to be processed.
            spider (Spider): spider that crawled the item.
        """
        pass

    def close(self) -> None:
        """Perform the cleanup if necessary."""
        pass


from spiderino.pipelines.csv_exporter import CsvExporter  # noqa
from spiderino.pipelines.json_exporter import JsonExporter  # noqa

__all__ = ['BasePipeline', 'CsvExporter']
