import json
from datetime import datetime
from typing import TYPE_CHECKING

from spiderino.pipelines import BasePipeline

if TYPE_CHECKING:
    from spiderino import Item, Spider


class JsonExporter(BasePipeline):
    """Export items to a json file."""
    def __init__(self, loop, config, **kwargs):
        super().__init__(loop, config)
        self._first_item = True
        self.filename = f'result - {datetime.now()}.json'
        self.file = open(self.filename, 'w', newline='')
        self.file.write('[\n\t')

    async def process_item(self, item: 'Item', spider: 'Spider') -> None:
        if self._first_item:
            self.file.write(json.dumps(item.as_dict))
            self._first_item = False
        else:
            self.file.write(',\n\t' + json.dumps(item.as_dict))

    def close(self) -> None:
        self.file.write('\n]')
