import logging

from spiderino.fields import Field

__all__ = ["Item"]

log = logging.getLogger(__name__)


class ItemMeta(type):
    def __new__(cls, name, bases, attrs):
        for k, v in attrs.items():
            if isinstance(v, Field):
                v.label = k

        return super(ItemMeta, cls).__new__(cls, name, bases, attrs)


class Item(metaclass=ItemMeta):
    pass
