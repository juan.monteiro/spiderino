from abc import ABCMeta, abstractmethod
from typing import TYPE_CHECKING, AsyncIterable, List

if TYPE_CHECKING:
    from aiohttp.web import Response
    from yarl import URL

    from spiderino import Crawler, Item


class Spider(metaclass=ABCMeta):
    """Base spider class."""

    name: str = None
    start_urls: List['URL'] = None

    def __init__(self):
        self._crawler = None

    def set_crawler(self, crawler: 'Crawler'):
        self._crawler = crawler

    @abstractmethod
    async def process(self, response: 'Response') -> AsyncIterable['Item']:
        """Process the response and yield items"""
        yield None

    def follow(self, url: 'URL') -> None:
        """Queue a url to be crawled."""
        self._crawler.crawl(url, self)
