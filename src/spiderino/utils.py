from importlib import import_module


def import_from_str(path: str):
    """Import from a dotted path and return a attribute/class.

    Args:
        path: A dotted path eg: 'spiderino.downloader.Downloader'
    """
    try:
        module_path, klass = path.rsplit('.', 1)
        module = import_module(module_path)
        return getattr(module, klass)
    except:
        raise ImportError(f'Failed to import {path}')
