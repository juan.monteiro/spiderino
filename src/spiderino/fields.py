__all__ = ["BooleanField", "FloatField", "IntegerField", "StringField"]


class Field(object):
    def __init__(self, default=None, _item=None):
        self.label = None

    def __get__(self, instance, owner):
        return instance.__dict__.get(self.label, None)

    def __set__(self, instance, value):
        self._validade(value)
        instance.__dict__[self.label] = value

    def _validade(self, value):
        raise NotImplementedError


class BooleanField(Field):
    def _validade(self, value):
        if not isinstance(value, bool):
            raise ValueError("Value must be a bool.")


class FloatField(Field):
    def _validade(self, value):
        if not isinstance(value, float):
            raise ValueError("Value must be float")


class IntegerField(Field):
    def _validade(self, value):
        if not isinstance(value, int):
            raise ValueError("Value must be int.")


class StringField(Field):
    def _validade(self, value):
        if not isinstance(value, str):
            raise ValueError("Value must be str.")
