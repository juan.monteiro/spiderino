"""
    spiderino
    ~~~~~~~~~

    A simple web crawling framework built with asyncio.

    :copyright: (c) 2017 by Juan Carlo Domingues Monteiro.
    :license: MIT, see LICENSE for more details.
"""
__version__ = "0.1-dev"

from yarl import URL

from spiderino.crawler import Crawler
from spiderino.item import Item
from spiderino.spider import Spider

__all__ = ['Crawler', 'Item', 'Spider', 'URL']
