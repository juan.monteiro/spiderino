import ast
import re

from setuptools import find_packages, setup

with open('src/spiderino/__init__.py', 'rb') as f:
    VERSION = str(ast.literal_eval(re.compile(r'__version__\s+=\s+(.*)').search(
        f.read().decode('utf-8')).group(1)))


setup(
    name="spiderino",
    description="A simple web crawling framework built with asyncio.",
    license="MIT",
    url="http://www.jmonteiro.net/project/aio-crawler",
    version=VERSION,
    author="Juan Monteiro",
    author_email="juan.monteiro@protonmail.ch",
    packages=find_packages(where="src"),
    package_dir={"": "src"},
    zip_safe=False,
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: Implementation :: CPython",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ],
    install_requires=[
        'aiohttp>=2.0.6',
        'yarl>=0.10'
    ]
)
